pub struct EquationSystem {
    pub coefficients: Vec<Vec<f64>>,
    pub constants: Vec<f64>,
}

// TODO: Check whether the matrix is square
// TODO: Check if the matrix is diagonally dominant
// REVIEW: Create a trait for various solution methods, including one for the Gauss–Seidel method
impl EquationSystem {
    pub fn solve(&self) -> Vec<f64> {
        // FIXME: Get the equations number somehow else
        let equations_number = self.constants.len();
        let mut previous_solution;
        let mut solution = vec![0f64; equations_number];

        // FIXME: Pass iteration number in method parameters
        for _iteration in 1..10000 {
            previous_solution = solution.clone();
            // REVIEW: To use or not to use these ‘i’, ‘j’ useless variable names
            for i in 0..equations_number {
                // FIXME: Rename the ‘var’ variable
                let mut var = 0f64;
                for j in 0..i {
                    var += self.coefficients[i][j] * solution[j];
                }
                for j in (i + 1)..equations_number {
                    var += self.coefficients[i][j] * previous_solution[j];
                }
                solution[i] = (self.constants[i] - var) / self.coefficients[i][i];
            }
        }

        return solution;
    }
}
