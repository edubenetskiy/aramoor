extern crate aramoor;
extern crate cursive;

use cursive::Cursive;
use cursive::traits::*;
use cursive::views::*;

use std::fs::OpenOptions;
use std::path::Path;
use std::io::Read;

use aramoor::parser;

// HACK: Eliminate LoadOption and use closures instead
enum LoadOption {
    LoadFromFile,
    EnterFromKeyboard,
    GenerateRandomCoefficients,
}

fn main() {
    let mut cursive = Cursive::new();
    show_main_dialog(&mut cursive);
    cursive.run();
}

fn load_from_file(s: &mut Cursive) {
    fn submit(s: &mut Cursive, filename: &str) {
        if filename.is_empty() {
            s.add_layer(Dialog::text("Имя файла не может быть пустым.")
                .title("Ошибка")
                .dismiss_button("Ладно"));
            return;
        }
        let path = Path::new(filename.into());
        // TODO: Display a message if could not read file
        if let Ok(mut file) = OpenOptions::new().read(true).open(path) {
            let mut buf = String::with_capacity(4096); // HACK
            file.read_to_string(&mut buf).ok();
            if let Ok(system) = parser::parse(buf) {
                let solution = system.solve();
                s.add_layer(Dialog::info(format!("Файл ‘{}’ загружен.\nРешение: {:?}", filename, solution)));
            } else {
                s.add_layer(Dialog::text("Неверный формат файла.")
                    .title("Ошибка")
                    .dismiss_button("Ладно"));
            }
        } else {
            s.add_layer(Dialog::text(format!("Не удалось открыть файл ‘{}’", filename))
                .title("Ошибка")
                .dismiss_button("Ладно"));
        }
    }

    s.add_layer(Dialog::new()
        .title("Введите имя файла")
        .padding_top(1)
        .content(EditView::new()
            .on_submit(submit)
            .with_id("filename")
            .min_width(25))
        .dismiss_button("Назад")
        .button("OK", |s| {
            let filename = s.call_on_id("filename", |v: &mut EditView| v.get_content()).unwrap();
            submit(s, &filename);
        }));
}

fn show_main_dialog(s: &mut Cursive) {
    s.add_layer(Dialog::around(LinearLayout::vertical()
        .child(DummyView)
        .child(TextView::new("Выберите дальнейшее действие:").center())
        .child(DummyView)
        .child(SelectView::new()
            .with_all(vec![
                ("Загрузить систему из файла", LoadOption::LoadFromFile),
                ("Ввести коэффициенты с клавиатуры", LoadOption::EnterFromKeyboard),
                ("Сгенерировать случайные коэффициенты", LoadOption::GenerateRandomCoefficients)
            ])
            .on_submit(|s, p| {
                match *p {
                    LoadOption::LoadFromFile => load_from_file(s),
                    LoadOption::EnterFromKeyboard => enter_from_keyboard(s),
                    LoadOption::GenerateRandomCoefficients => generate_random_coefficients(s),
                }
            })))
        .title("Добро пожаловать!")
        .button("О программе", show_about_dialog)
        .button("Выйти", Cursive::quit));
}

fn show_about_dialog(s: &mut Cursive) {
    s.add_layer(Dialog::text(
        "Aramoor — программа для решения систем\n\
         линейных алгебраических уравнений.\n\n\
         © Егор Дубенецкий, 2017")
        .padding_top(1)
        .title("О программе")
        .dismiss_button("Понятно"))
}

fn enter_from_keyboard(s: &mut Cursive) {
    s.add_layer(Dialog::new()
        .title("Введите коэффициенты")
        .padding_top(1)
        .content(LinearLayout::vertical()
            .child(TextView::new("Число уравнений:"))
            .child(EditView::new()
                .content("3")
                .with_id("order"))
            .child(TextView::new("Коэффициенты:"))
            .child(TextArea::new()
                .with_id("coefficients")
                .min_size((25, 10)))
            .child(TextView::new("Столбец свободных членов:"))
            .child(EditView::new()
                .with_id("constants")))
        .dismiss_button("Назад")
        .button("Очистить", |s| {
            s.call_on_id("coefficients", |k: &mut TextArea| { k.set_content(""); });
            s.call_on_id("constants", |k: &mut TextArea| { k.set_content(""); });
            // TODO: Focus some text area if possible
        })
        .button("OK", |s| {
            let order = s.call_on_id("order", |k: &mut EditView| k.get_content()).unwrap();
            let coefficients = s.call_on_id("coefficients", |k: &mut TextArea| k.get_content().to_owned()).unwrap();
            let constants = s.call_on_id("constants", |k: &mut EditView| k.get_content()).unwrap();
            let contents = format!("{}\n{}\n{}\n", order, coefficients, constants);
            if let Ok(system) = parser::parse(contents) {
                let solution = system.solve();
                s.add_layer(Dialog::info(format!("Решение:\n{:?}", solution)));
            } else {
                // TODO: Tell user about input format
                s.add_layer(Dialog::text("Неверный формат ввода.")
                    .title("Ошибка")
                    .dismiss_button("Ладно"))
            }
        }));
}

// TODO
fn generate_random_coefficients(s: &mut Cursive) {
    s.add_layer(Dialog::text("Эта функция ещё не реализована.")
        .title("Извините!")
        .dismiss_button("Ладно"));
}
