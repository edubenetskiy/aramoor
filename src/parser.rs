use std::str::FromStr;
use equation_system::EquationSystem;

// TODO: Refactor this shit
pub fn parse<S: Into<String>>(string: S) -> Result<EquationSystem, ()> {
    let string = string.into();
    let tokens: Vec<Option<f64>> = string.split_whitespace().map(|s| f64::from_str(s).ok()).collect();
    if tokens.iter().any(Option::is_none) {
        return Err(());
    }
    let mut tokens = tokens.iter().map(|o| o.unwrap());

    if let Some(number) = tokens.next() {
        let size = number as usize;
        let mut tokens: Vec<f64> = tokens.collect();
        if tokens.len() < size * size + size {
            return Err(());
        }
        // HACK: Get rid of this ‘reverse & pop’ crutch
        tokens.reverse();
        let coefficients: Vec<Vec<f64>> = (0..size).map(|_| {
            (0..size).map(|_| {
                tokens.pop().unwrap()
            }).collect()
        }).collect();
        let constants: Vec<f64> = (0..size).map(|_| tokens.pop().unwrap()).collect();
        Ok(EquationSystem { coefficients, constants })
    } else {
        Err(())
    }
}
