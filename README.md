# Aramoor

Yet another linear system solver with a text user interface.

Written in [Rust][] with the [cursive][] crate which provides the `ncurses`
interface.

## Screenshot

![Aramoor Screenshot](https://gitlab.com/edubenetskiy/aramoor/raw/develop/screenshot.png)

[Rust]: https://rust-lang.org
[cursive]: https://crates.io/crates/cursive
